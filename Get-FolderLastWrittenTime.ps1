﻿function Get-FolderLastWrittenTime() {
    <#
    .SYNOPSIS
    Cycles through a folder and returns the last file that was modified in the folder.

    .NOTES
    @author Hashaw


    #>
    [CmdletBinding()] param(
        [string]$Folder
    )
    

    #First step, get all items in folder
    $files = Get-ChildItem -Path $Folder -Filter *
    try {
        $latestWriteTime = $files[0]
    } catch {
        return "Folder was empty"
    }
    $files | ForEach-Object {
        if ($latestWriteTime.LastWriteTime -lt $_.LastWriteTime){
            $latestWriteTime = $_
        }

    }

    return $latestWriteTime
}