﻿function Set-ADPhoneNumbers() {
    <#
    .SYNOPSIS
    Sets Active Directory phone numbers

    .DESCRIPTION
    Sets phone numbers for both mobile and office phones per user. Can either take the user's full name or the SAM account name


    .PARAMETER Name
    Can either take the user's Username, or the user's Full name. Must be one of the two.


    .PARAMETER MobilePhone
    User's mobile phone number

    
    .PARAMETER OfficePhone
    User's cell phone number


    .EXAMPLE


    .NOTES
    @author Hashaw
    @email  Hashaw@sithlord.ca


    #>
    [CmdletBinding()] param(
        [Parameter(Mandatory=$True)]
        [string] $Name,
        [string] $MobilePhone = $null,
        [string] $OfficePhone = $null
    )

    Import-Module ActiveDirectory

    $SplitName = $Name -split " "

    if (!$MobilePhone -and !$OfficePhone) {
        throw "No phone numbers to set"
    }

    if ($SplitName.Length -gt 1) {
        Write-Debug "Full Name Detected"
        $GivenName = $SplitName[0]
        $Surname = $SplitName[1]
        $samName  = (Get-ADUser -Filter {GivenName -eq $GivenName -and Surname -eq $Surname} | Select-Object samAccountName).SamAccountName

        #This could be turned into a subfunction, however I get bizarre errors when I do.
        if($MobilePhone -and $OfficePhone) {
            Set-ADUser -Identity $samName -MobilePhone $MobilePhone -OfficePhone $OfficePhone
            return $Name + ": Mobile phone and Office phone have been updated"
        } ElseIf ($MobilePhone) {
            Set-ADUser -Identity $samName -MobilePhone $MobilePhone
            return $Name + ": Mobile phone has been updated"
        } ElseIf ($OfficePhone) {
            Set-ADUser -Identity $samName -OfficePhone $OfficePhone
            return $Name + ": Office phone has been updated"
        } Else {
            return "Nothing has been updated for user: $Name"
        }

    } ElseIf ($SplitName.Length -eq 1) {
        Write-Debug "SamAccountName detected"
        $samName = $Name

        if($MobilePhone -and $OfficePhone) {
            Set-ADUser -Identity $samName -MobilePhone $MobilePhone -OfficePhone $OfficePhone
            return $Name + ": Mobile phone and Office phone have been updated"
        } ElseIf ($MobilePhone) {
            Set-ADUser -Identity $samName -MobilePhone $MobilePhone
            return $Name + ": Mobile phone has been updated"
        } ElseIf ($OfficePhone) {
            Set-ADUser -Identity $samName -OfficePhone $OfficePhone
            return $Name + ": Office phone has been updated"
        } Else {
            return "Nothing has been updated for user: $Name"
        }

        

    }


  
}