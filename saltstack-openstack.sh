yum install http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm -y
yum install http://rdo.fedorapeople.org/openstack-kilo/rdo-release-kilo.rpm -y
systemctl disable NetworkManager
systemctl disable firewalld
systemctl stop NetworkManager
systemctl stop firewalld
yum install wget -y
wget -O install_salt.sh https://bootstrap.saltstack.com
sudo sh install_salt.sh
sudo sh -c "echo 'master: 10.150.25.165' >> /etc/salt/minion"
sudo sh -c "echo 'log_file: /var/log/salt/minion' >> /etc/salt/minion"
sudo sh -c "echo 'log_level_logfile: debug' >> /etc/salt/minion"
service salt-minion restart
yum update -y && yum upgrade