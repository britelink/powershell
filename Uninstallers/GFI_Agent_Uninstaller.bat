:::::::::::::::::::::::::::::::::::::::::
:: Automatically check & get admin rights
:::::::::::::::::::::::::::::::::::::::::
@echo off
CLS 
ECHO.
ECHO =============================
ECHO Running Admin shell
ECHO =============================

:checkPrivileges 
NET FILE 1>NUL 2>NUL
if '%errorlevel%' == '0' ( goto gotPrivileges ) else ( goto getPrivileges ) 

:getPrivileges 
if '%1'=='ELEV' (shift & goto gotPrivileges)  
ECHO. 
ECHO **************************************
ECHO Invoking UAC for Privilege Escalation 
ECHO **************************************

setlocal DisableDelayedExpansion
set "batchPath=%~0"
setlocal EnableDelayedExpansion
ECHO Set UAC = CreateObject^("Shell.Application"^) > "%temp%\OEgetPrivileges.vbs" 
ECHO UAC.ShellExecute "!batchPath!", "ELEV", "", "runas", 1 >> "%temp%\OEgetPrivileges.vbs" 
"%temp%\OEgetPrivileges.vbs" 
exit /B 

:gotPrivileges 
::::::::::::::::::::::::::::
:START
::::::::::::::::::::::::::::
setlocal & pushd .

REM Run shell as admin (example) - put here code as you like

IF NOT EXIST "C:\Program Files\Advanced Monitoring Agent\unins000.exe" GOTO NEXT1
"C:\Program Files\Advanced Monitoring Agent\unins000.exe" /silent
ECHO Uninstalled from C:\Program Files\Advanced Monitoring Agent\
GOTO END


:NEXT1

ECHO Not Installed in C:\Program Files\Advanced Monitoring Agent\

IF NOT EXIST "C:\Program Files (x86)\Advanced Monitoring Agent\unins000.exe" GOTO NEXT2
"C:\Program Files (x86)\Advanced Monitoring Agent\unins000.exe" /silent
ECHO Uninstalled from C:\Program Files (x86)\Advanced Monitoring Agent\
GOTO END


:NEXT2

ECHO Not Installed in C:\Program Files (x86)\Advanced Monitoring Agent\

wmic product where name="Advanced Monitoring Agent GP" call uninstall /nointeractive


:END
cmd /k