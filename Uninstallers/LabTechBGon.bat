@echo off

echo Stopping LT services...
sc stop "LTservice"
sc stop "LTSsvcMon"
Echo Done.

echo uninstalling services...
sc delete "LTservice"
sc delete "LTSvcMon"
echo Done.

echo deleting from registry...
REG DELETE HKEY_LOCAL_MACHINE\Software\LabTech /f
REG DELETE HKEY_LOCAL_MACHINE\Software\Wow6432Node\LabTech /reg:64 /f

echo Done.

echo Killing process...
Taskkill /IM LTTray.exe /F
echo Done.

echo Deleting executable...
rmdir  C:\windows\LTsvc /S /Q
rmdir  C:\windows\LTsvc.old /S /Q
Echo Done.

Echo Uninstall completed, please reboot PC at earliest convenience
