﻿function Set-AbaqusLicense() {

    <#
    .SYNOPSIS
    Sets the Dassault Systems Abaqus program's license server.

    .DESCRIPTION
    The Set-AbaqusLicense function recursively loops through directories looking for the v6 environment file, then modifies the file with the new license server location.

    .PARAMETER Drive
    Specifies the drive to check for Abaqus on the target server.
    Default C:\

    .PARAMETER NewAbaqusServer
    Specifies the new Abaqus License Server hostname
    
    .PARAMETER Port
    Specifies the port that the License Server is listening on. 
    Default 27000

    .EXAMPLE
    Set-AbaqusLicense -Drive C:\ -NewAbaqusServer New-DCServer01 -Port 27000

    .NOTES
    @author Hashaw


    #>
    [CmdletBinding()] param(
        [string]$Drive = 'c:\', 
        [string]$NewAbaqusServer,
        [int]$Port = 27000
    )
    
    
    $File = Get-ChildItem $Drive -Recurse -Filter "abaqus_v6.env"
    # Set this because I can't find a more elegant way to pass the file's location

    $File | % {
    $FileLocation = $File.DirectoryName + "\" + $File.Name

    $FileContent = $File | Get-Content | % {$_ -replace "abaquslm_license_file=.+", "abaquslm_license_file=`"$Port@$NewAbaqusServer`""}

    $FileContent | Set-Content $FileLocation
    }

}


