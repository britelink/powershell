﻿function Get-ServicesRunningAsGroup() {
    <#
    .SYNOPSIS
    Returns services that are being run under accounts in group

    .DESCRIPTION
    Takes in a User Group name and a Target and returns every service running on target computer
    which is running under a user account in that user group. Since this script pulls from Active Directory, it does not pull local accounts.


    .PARAMETER UserGroup
    The Usergroup that you want to scan for users running services. For example, "Domain Admins". Only points to Active Directory users


    .PARAMETER Computer
    The computer you want to call

    .EXAMPLE
    Get-ServicesRunningAsGroup -Computer Server-DC1 -UserGroup "Domain Admins"

    .NOTES
    @author Hashaw
    Based on script found on the internet whose origin I cannot remember


    #>
    [CmdletBinding()] param(
        [Parameter(Mandatory=$true)]
        [string] $UserGroup = $null,
        [string] $Computer =$null
    )
    Import-Module ActiveDirectory

    if(!$Computer) {
        $Computer = $env:COMPUTERNAME
    }

    $users = Get-ADGroupMember $UserGroup | Get-ADUser
    $domainName = (Get-ADDomain).name

    foreach ($user in $users) {
        Try {
        $output = Get-WMIObject Win32_Service -ComputerName $Computer | Where-Object {$_.StartName -eq "$domainName\$($user.SamAccountName)" }
        } Catch [UnauthorizedAccessException] {
            Write-Error $_.Exception.Message
            throw "You are not authorized to pull information from Active Directory. Try re-running as an account with those permissions"
        }
        if (!$output) {
            Write-Debug "No services running under account: $user.SamAccountName"
        }
        elseif ($output) {
            Write-Output $output
        }
    }
  }
