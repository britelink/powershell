﻿param(
    [int]$CustomerID
        
)

Write-Output "Stopping Agent... - Line 6"
$agent = Get-WmiObject win32_process -Filter "name = 'Agent.exe'"
$agentMaint = Get-WmiObject win32_process -Filter "name = 'AgentMaint.exe'"
$agent.Terminate()
$agentMaint.Terminate()
Get-Process Agent* | Stop-Process -Force

Write-Output "Stopping NableReactiveManagement... - Line 13"
$nAble = Get-WmiObject win32_process -Filter "name = 'NableReactiveManagement.exe'"
$nAbleMan = Get-WmiObject win32_process -Filter "name = 'NableSixtyFourBitManager.exe'"
$nAble.Terminate()
$nAbleMan.Terminate()
Get-Process Nable* | Stop-Process -Force

Write-Output "Stopping Services... - Line 20"
$Services = Get-Service -Name "Windows Agent*"
$Services | Stop-Service -Force

Write-Output "Acquiring XML files... - Line 24"
$File = Get-ChildItem 'C:\Program Files (x86)\N-able Technologies\Windows Agent' -Recurse -Filter "applianceconfig.xml"

# Set this because I can't find a more elegant way to pass the file's location

Write-Output "Modifying XML files... - Line 29"
foreach ($obj in $File) {
    $FileLocation = $obj.DirectoryName + "\" + $obj.Name
    $FileContent = $obj | Get-Content

    $FileContent = $FileContent -replace "<CustomerID>.+", "<CustomerID>$CustomerID</CustomerID>"
    $FileContent = $FileContent -replace "<ApplianceID>.+", "<ApplianceID>-1</ApplianceID>"
    $FileContent = $FileContent -replace "<CheckerLogSent>.+", "<CheckerLogSent>False</CheckerLogSent>"
    $FileContent = $FileContent -replace "<CompletedRegistration>.+", "<CompletedRegistration>False</CompletedRegistration>"

    $FileContent | Set-Content $FileLocation
}

Write-Output "Starting services... - Line 42"
$Services | Start-Service

Write-Output "Starting Sleep... - Line 45"
Start-Sleep -s 300
Write-Output "Done Sleeping... - Line 47"

Write-Output "Stopping NableReactiveManagement... - Line 49"
$nAble.Terminate()
$nAbleMan.Terminate()
Get-Process Nable* | Stop-Process -Force

Write-Output "Stopping Agent... - Line 56"
$agent.Terminate()
$agentMaint.Terminate()
Get-Process Agent* | Stop-Process -Force

Write-Output "Stopping Services... - Line 63"
$Services = Get-Service -Name "Windows Agent*"
$Services | Stop-Service -Force

Write-Output "Acquiring XML files... - Line 67"
$ReactiveFile = Get-ChildItem 'C:\Program Files (x86)\N-able Technologies\Reactive' -Recurse -Filter "applianceconfig.xml"
$File = Get-Content 'C:\Program Files (x86)\N-able Technologies\Windows Agent\config\ApplianceConfig.xml'


Write-Output "Modifying XML files... - Line 72"
foreach ($obj in $ReactiveFile) {
    $FileLocation = $obj.DirectoryName + "\" + $obj.Name
    $FileContent = $obj | Get-Content
    $applianceID = [string]($File -match "<applianceID>" )
    $FileContent = $FileContent -replace "<CustomerID>.+", "<CustomerID>$CustomerID</CustomerID>"
    $FileContent = $FileContent -replace "<ApplianceID>.+", $applianceID.toString()
    $FileContent = $FileContent -replace "<CheckerLogSent>.+", "<CheckerLogSent>False</CheckerLogSent>"
    $FileContent = $FileContent -replace "<CompletedRegistration>.+", "<CompletedRegistration>False</CompletedRegistration>"

    $FileContent | Set-Content $FileLocation

    }

Write-Output "Starting services... - Line 86"
$Services | Start-Service

Write-Output "Restarting services... - Line 89"
$Services | Restart-Service
