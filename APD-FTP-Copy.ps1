﻿#Requires -Version 5.0
Param (
    [string]$userName,
    [string]$password,
    [string]$hostServer,
    [string]$targetFolder,
    [int]   $portNumber,
    [string]$hostTLSFingerprint

)

if (Get-Module -ListAvailable -Name WinSCP) {
    Import-Module WinSCP
} else {
    Write-Warning "Required module WinSCP is not installed on system. Installing now."
    Try {
        Install-Module -Name "WinSCP"
    } Catch {
        Write-Error "Required module WinSCP could not be found and was unable to be installed. Program is now terminating."
        exit 1
    }
}

$transferFiles = Get-ChildItem -Recurse $targetFolder | where { ! $_.PSIsContainer }
$secpasswd = ConvertTo-SecureString $password -AsPlainText -Force
$Credentials = New-Object System.Management.Automation.PSCredential ($userName, $secpasswd)
$session = New-WinSCPSession -Protocol Ftp -HostName $hostServer -PortNumber $portNumber -FtpSecure Explicit -Credential $Credentials -TlsHostCertificateFingerprint $hostTLSFingerprint
$transferFiles | ForEach-Object {
    Write-Debug $_.DirectoryName
    $fileLoc      = $_.DirectoryName.Tostring() + "\"
    $fileName     = $_.Name.toString()
    $fileToMove   = $fileLoc + $fileName
    $remoteFolder = $fileLoc.Substring($targetFolder.Length) -replace "\\", "/"
    try {
        if (Test-Path -Path $fileToMove) {
            if (!($session.FileExists($remoteFolder))){
                $session.CreateDirectory($remoteFolder)
            }
            if (!($fileName.Substring(($fileName.Length -3), 3) -eq "zip")){
                Compress-Archive -Path $fileToMove -CompressionLevel Optimal -DestinationPath ($fileToMove + ".zip") -Force
                Send-WinSCPItem -Path ($fileToMove + ".zip") -WinSCPSession $session -Remove -Destination ($remoteFolder + $fileName + ".zip")
            } else {
                Send-WinSCPItem -Path ($fileToMove) -WinSCPSession $session -Remove -Destination ($remoteFolder + $fileName + ".zip")
            }
        }
    } Catch {
        Write-Error $Error[0]
        exit 1
    }
    If(Test-Path -LiteralPath $fileToMove) {
        Remove-Item -LiteralPath $fileToMove -Force
    }
}
Remove-WinSCPSession
exit 0