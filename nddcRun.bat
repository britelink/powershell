@echo off
cd c:\Temp\NDDC\
mkdir Completed
mkdir Util
Set location=C:\Temp\NDDC\


%location%\Util\pageant.exe %location%\Util\takemehome.ppk

%location%\Util\NetworkDetectiveDataCollector.exe /auto %location%
TaskKill /IM RunNetworkDetective.exe /F

RunNetworkDetective.exe -file %LOCATION%\util\default.ndp -outdir %location%\Completed

(
echo mkdir %USERDOMAIN%
echo cd %USERDOMAIN%
echo put -r Completed
echo quit
) | util\psftp Jeeves@127.0.0.1 -bc

rem Clean up
rem only for when the script is working
rem taskkill /im pageant.exe /f
rem rmdir /S /Q %location%
