﻿function Set-AnsysLicense() {

    <#
    .SYNOPSIS
    Sets the ANSYS program's license

    .DESCRIPTION
    The Set-AnsysLicense function recursively loops through directories looking for the Ansys license file, then modifies the file with the new license server location.

    .PARAMETER Drive
    Specifies the drive to check for Ansys on the target server.
    Default C:\

    .PARAMETER NewAnsysServer
    Specifies the new Ansys License Server hostname

    .EXAMPLE
    Set-AnsysLicense -Drive C:\ -NewAnsysServer New-DCServer01 -Port 27000

    .NOTES
    @author Hashaw


    #>
    [CmdletBinding()] param(
        [string]$Drive = 'c:\', 
        [string]$NewAnsysServer
    )
    
    
    $File = Get-ChildItem $Drive -Recurse -Filter "ansyslmd.ini"

    $File | % {
    # Set this because I can't find a more elegant way to pass the file's location
    $FileLocation = $File.DirectoryName + "\" + $File.Name

    $FileContent = $File | Get-Content | % {$_ -replace "SERVER=.+", "SERVER=1055@$NewAnsysServer" -replace "ANSYSLI_SERVERS=.+", "ANSYSLI_SERVERS=2325@$NewAnsysServer"}

    $FileContent | Set-Content $FileLocation
    }

}


