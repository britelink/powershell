﻿$javapath = "C:\Windows\Sun\Java\Deployment"

if (!(Test-Path $javapath)) {
    New-Item -ItemType directory $javapath
} 

"deployment.system.config.mandatory=True" > $javapath\deployment.config
"deployment.system.config=file\:C\:/WINDOWS/Sun/Java/Deployment/deployment.properties" >> $javapath\deployment.config

"deployment.security.SSLv2Hello=false" > $javapath\deployment.properties
"deployment.security.SSLv2Hello.locked" >> $javapath\deployment.properties