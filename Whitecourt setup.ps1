﻿mkdir "C:\Support"
cd "C:\Support"

$group = [ADSI]"WinNT://./Network Configuration Operators,group"
$group.Add("WinNT://User,user")
$group = [ADSI]"WinNT://./Users,group"
$group.Add("WinNT://User,user")
$adminGroup = [ADSI]"WinNT://./Administrators,group"
$adminGroup.Remove("WinNT://User,user")
Set-ExecutionPolicy RemoteSigned -Force
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")

cinst dotnet4.5.2 -y
cinst powershell -y
cinst jre8 -y 
cinst adobereader -y --allow-empty-checksums
#Subscribes to all updates for Microsfot Windows Update
$mu = New-Object -ComObject Microsoft.Update.ServiceManager -Strict 
$mu.AddService2("7971f918-a847-4430-9279-4a52d1efe18d",7,"")

cd C:\Users\Administrator\Desktop\office2010
Start-Process .\setup.exe -ArgumentList "/adminfile Updates/Outlier.MSP" -Wait
cscript “C:\Program Files (x86)\Microsoft Office\Office14\OSPP.VBS” /act
Write-Output "All finished! Please reboot"