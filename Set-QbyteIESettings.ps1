﻿function Set-WebsiteTrusted() {
    <#
    .SYNOPSIS
    Adds website to trusted site list in IE

    .DESCRIPTION
    Adds website to trusted site list in IE

    .PARAMETER website
    Website you want to add to the trusted sites list

    .EXAMPLE
    Set-WebsiteTrusted -website "qbyte.com"

    .NOTES
    @author Hashaw
    Code stolen from: http://stackoverflow.com/questions/27359636/change-internet-explorer-security-settings-for-trusted-domains-using-powershell

    #>
    [CmdletBinding()] param(
        [string]$website

    )
    
    #Setting IExplorer settings
    Write-Verbose "Now configuring IE for $website"
    #Add http://website.com as a trusted Site/Domain
    #Navigate to the domains folder in the registry
    set-location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    set-location ZoneMap\Domains

    #Create a new folder with the website name
    new-item "$website/" -Force
    set-location "$website/"
    new-itemproperty . -Name * -Value 2 -Type DWORD -Force
    new-itemproperty . -Name http -Value 2 -Type DWORD -Force
    new-itemproperty . -Name https -Value 2 -Type DWORD -Force

}

$websites = @(
"qbyte.com",
"qbasp.com",
"p2es.com",
"p2energysolutions.com"
)

$websites | % { Set-WebsiteTrusted -website $_ }

Set-Location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings\zones\2"
#Enable is 0, disable is 1, prompt is sometimes 2

#ActiveX controls and plug-ins: Download unsigned ActiveX controls
new-itemproperty . -Name 1004 -Value 1 -Type DWORD -Force
#ActiveX controls and plug-ins: Initialize and script ActiveX controls not marked as safe for scripting
new-itemproperty . -Name 1201 -Value 1 -Type DWORD -Force
#ActiveX controls and plugins: Run ActiveX controls and plug-ins
new-itemproperty . -Name 1200 -Value 0 -Type DWORD -Force
#Automatic prompting for file downloads
new-itemproperty . -Name 2200 -Value 0 -Type DWORD -Force
#Downloads: File Download
new-itemproperty . -Name 1803 -Value 0 -Type DWORD -Force
