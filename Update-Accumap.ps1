﻿function UnZip($directory, $fileToUnzip){
    $7Zip = 'C:\Support\Accumap Update\7z\7z.exe'
    $newName = $fileToUnzip.Substring(0, $fileToUnzip.Length -3) +"rar"
    Rename-Item -Path ($directory + "\" + $fileToUnzip) -NewName $newName -Force
    & $7zip x -aos ("-o" + $directory) -bb0 -pdefault -sccUTF-8 ($directory + "\" + $newName) -r -- "*" | Out-Null
}

function ModifyISS($issFile, $versionNumber) {
    $FileContent = Get-Content $issFile | % {$_ -replace $versionRegex, $versionNumber}

    $FileContent | Set-Content $issFile -Force

}

function Install($directory, $mainExecutable) {
    Start-Process -FilePath ($directory +"\" + $mainExecutable) -ArgumentList "/s /sms" -Wait
}

function cleanUp($directory) {
    Remove-Item $directory -Force -Recurse
    Write-Output $dateCollection | Export-Csv -Path "C:\Support\Accumap Update\Last Updated.csv" -NoTypeInformation -Force

}


$url      = "http://accumapupdates.ihsenergy.com/"
$dateRegex    = [regex]"((.\w{2,3}) \d+ \d\d\d\d)"
$versionRegex = [regex]"([vV]\d\d.\d\d\w|v\d\d.\d\d)"
$utilsDir     = "C:\Support\Accumap Update\"
$tempDir      = "C:\Support\Accumap Update\Temp"
$monthlyVer
[array]$toUpdate = @()
$monthToNum = @{
    "Jan"  = 1
    "Feb"  = 2
    "Mar"  = 3
    "Apr"  = 4
    "May"  = 5
    "June" = 6
    "Jun"  = 6
    "July" = 7
    "Jul"  = 7
    "Aug"  = 8
    "Sep"  = 9
    "Sept" = 9
    "Oct"  = 10
    "Nov"  = 11
    "Dec"  = 12
}

try {
    New-Item -ItemType Directory -Path $tempDir -Force
} catch {
    Write-Error "Unable to create necessary directories"
    exit 69002
}

try {
    $r = Invoke-WebRequest $url
} catch {
    Write-Error "Unable to access Accumap website"
    exit 69001
}

## Normalize the data
[array]$dateCollection = @()

$numToName = @("Accumap", "weekly", "Land Text", "TID", "Extended Culture and Contours", "Acculogs")

$dateMatches = $dateRegex.Matches($r.RawContent)
$versionMatches = $versionRegex.Matches($r.RawContent)

for ($i = 0; $i -lt $dateMatches.value.length; $i++) {
    $split = $dateMatches[$i].Value.split(" ", [System.StringSplitOptions]::RemoveEmptyEntries)
    $year  = [Int]$split[2]
    $month = [Int]$monthToNum[$split[0]]
    $day   = [Int]$split[1]
    if ($numToName[$i] -eq "Accumap" -or $numToName[$i] -eq "weekly") {
        $version = $versionMatches[$i].Value
	if ($numToName[$i] -eq "Accumap"){
	$monthlyVer = $versionMatches[$i].Value
	}
    } Else {
        $version = $null
    }
    $YearMonthDay = New-Object System.Object
    $YearMonthDay |Add-Member -MemberType NoteProperty -Name "Program" -Value $numToName[$i]
    $YearMonthDay |Add-Member -MemberType NoteProperty -Name "Year"    -Value $year
    $YearMonthDay |Add-Member -MemberType NoteProperty -Name "Month"   -Value $month
    $YearMonthDay |Add-Member -MemberType NoteProperty -Name "Day"     -Value $day
    $YearMonthDay |Add-Member -MemberType NoteProperty -Name "Version" -Value $version
    $YearMonthDay |Add-Member -MemberType NoteProperty -Name "Ignore"  -Value 0
    
    $dateCollection += $YearMonthDay

}

##check to see if there's any previous download information and make a list of things to update
if(!(Test-Path "C:\Support\Accumap Update")) {
    New-Item -ItemType Directory -Path "C:\Support\Accumap Update" -Force
} else {
    try {
        $lastUpdated = Import-Csv "C:\Support\Accumap Update\Last Updated.csv"

        for ($i = 0; $i -lt $lastUpdated.Length; $i++) {
            switch ($lastUpdated[$i]) {
                {$lastUpdated[$i].Ignore -eq 1}     {$dateCollection[$i].Ignore = 1; break}
                {$dateCollection[$i].Year -gt $_.Year}   {$toUpdate += $dateCollection[$i].Program; break}
                {$dateCollection[$i].Month -gt $_.Month} {$toUpdate += $dateCollection[$i].Program; break}
                {$dateCollection[$i].Day -gt $_.Day}     {$toUpdate += $dateCollection[$i].Program; break}
            }
        }
	if ($monthlyVer -and -not ($toUpdate -contains "TID")) {
        $toUpdate += "TID"
    }

    } catch {
        Write-Warning "Last Updated CSV was not present"
        for ($i = 0; $i -lt $dateCollection.Length; $i++) {
            $toUpdate += $dateCollection[$i].Program
        }
       
    }
}
#Downloading and unzip all the necessary files!
for ($i = 0; $i -lt $toUpdate.Length; $i++) {
    $accumapDir = $tempDir + "\" +$toUpdate[$i] + "\"
    New-Item -Path $accumapDir -Force -ItemType Directory
    switch($toUpdate[$i]) {  
        "Accumap" {
            $downloadUrls = @(
                "http://productdownloads.ihs.com/private/release/AccuSuite/HTTPNewAccuMap/NEWMAPIPL.part1.exe"
                "http://productdownloads.ihs.com/private/release/AccuSuite/HTTPNewAccuMap/NEWMAPIPL.part2.rar"
                "http://productdownloads.ihs.com/private/release/AccuSuite/HTTPNewAccuMap/NEWMAPIPL.part3.rar"
                "http://productdownloads.ihs.com/private/release/AccuSuite/HTTPNewAccuMap/NEWMAPIPL.part4.rar"
            )
            foreach ($url in $downloadUrls) {
                Start-BitsTransfer -Source $url -Destination $accumapDir -RetryTimeout 180
            }
            UnZip -directory $accumapDir -fileToUnzip "NEWMAPIPL.part1.exe"

            Copy-Item -Path ($utilsDir + "\ISS\Monthly.iss") -Destination ($accumapDir + "AccuMap\setup.iss")
            Install -directory ($accumapDir +"\Accumap\") -mainExecutable "_main.exe"
        
        }
        "weekly"  {
            $downloadUrls = @("http://accumapupdates.ihsenergy.com/HttpLand/IPLIHSIPL_Land_Update.exe")
            Start-BitsTransfer -Source $downloadUrls -Destination $accumapDir -RetryTimeout 180
            UnZip -directory $accumapDir -fileToUnzip "IPLIHSIPL_Land_Update.exe"
            Copy-Item -Path ($utilsDir + "\ISS\Weekly.iss") -Destination ($accumapDir + "\setup.iss")
	        Install -directory $accumapDir -mainExecutable "_main.exe"

        }
        "Land Text" {
            $downloadUrls = @("http://accumapupdates.ihsenergy.com/HttpLandText/Weekly_Land_Text_Update.exe")
            Start-BitsTransfer -Source $downloadUrls -Destination $accumapDir -RetryTimeout 180
            UnZip -directory $accumapDir -fileToUnzip "Weekly_Land_Text_Update.exe"
            if ($monthlyVer){
                ModifyISS -issFile ($utilsDir + "\ISS\LandText.iss") -versionNumber $monthlyVer
            }
            Copy-Item -Path ($utilsDir + "\ISS\LandText.iss") -Destination ($accumapDir + "\setup.iss")
	        Install -directory $accumapDir -mainExecutable "setup.exe"
        
        }
        "TID"       {
            $downloadUrls = @("http://accumapupdates.ihsenergy.com/TID/TID.exe")
            Start-BitsTransfer -Source $downloadUrls -Destination $accumapDir -RetryTimeout 180
            UnZip $accumapDir "TID.exe"
            if ($monthlyVer){
                ModifyISS ($utilsDir + "\ISS\TID.iss") $monthlyVer
            }
            Copy-Item -Path ($utilsDir + "\ISS\TID.iss") -Destination ($accumapDir + "\setup.iss")
	        Install -directory $accumapDir -mainExecutable "setup.exe"
        }
        #"Extended Culture and Contours" {} TODO
        #"Acculogs"  {} TODO
    }

}
#cleanUp -directory $tempDir
Write-Output "Updated ${toUpdate}"

