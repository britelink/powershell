﻿function Get-ServiceRunningAccount() {
    <#
    .SYNOPSIS
    Returns services that are being run under non-standard accounts

    .DESCRIPTION
    Takes in a User Group name and a Target and returns every service running on target computer
    which is not running under the default accounts of "LocalServer" "LocalSystem" and "NetworkService"


    .PARAMETER Computer
    The computer you want to call

    .EXAMPLE
    Get-ServiceRunningAccount -Computer Server-DC1

    .NOTES
    @author Hashaw
    Based on script found on the internet whose origin I cannot remember


    #>
    [CmdletBinding()] param(
        [string] $Computer = $null
    )
    if(!$Computer) {
        $Computer = $env:COMPUTERNAME
    }
        $output = Get-WMIObject Win32_Service -ComputerName $Computer | Where-Object {$_.StartName -ne "localSystem" -and  $_.StartName -ne "NT AUTHORITY\LocalService" -and $_.StartName -ne "NT AUTHORITY\NetworkService"}

        if (!$output) {
            Write-Output "No services running as non-standard account on computer $Computer"
        }
        elseif ($output) {
            Write-Output $output
        }
  }
