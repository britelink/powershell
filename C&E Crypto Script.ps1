﻿$ErrorActionPreference = "silentlycontinue"
Stop-Process -processname groove -Force
Stop-Process -processname msosync -Force
Stop-Process -processname msouc -Force
Stop-Process -processname csisyncclinet -Force
Stop-Process -processname winword -Force
Stop-Process -processname excel -Force
Stop-Process -processname powerpnt -Force
Stop-Process -processname mssync -Force

Remove-Item "$env:USERPROFILE\AppData\Local\Microsoft\Office\15.0\OfficeFileCache” -Recurse -Force
Remove-Item "$env:USERPROFILE\AppData\Local\Microsoft\Office\15.0\SPW” -Recurse -Force

Get-ChildItem -Path C:\Users -Attributes D -Recurse -Include *OneDrive*Cause* | Rename-Item -NewName "OneDrive Backup - Cause and Effect"

$SharePointNames = @(
"Budget",
"Dave's Subsite",
"Training Help",
"TOY Librarian",
"Speech Language",
"Physical Therapists", 
"Occupational Therapists", 
"FLCs", 
"Education Consultants",
"CDF Strategists",
"Behavior Strategists", 
"CDF Strategists", 
"Behavior Strategists",
"Wild West Team",
"Special Team",
"South West Team", 
"South East Team",
"Private Clients",
"Okotoks 2 Team", 
"Okotoks & Area Team",
"North West Team",
"North East Team",
"Mild Mod", 
"FSCD Caseloads",
"Foothills South Team",
"Central West Team",
"Bow Valley Team",
"CE Board",
"Administration",
"Human Resources",
"CDFs",
"Consultants",
"Wild West Team",
"Teacher Transition Documents",
"Cause and Effect"
)

$SharePoint = Get-ChildItem -Path C:\Users -Attributes D -Recurse -Include *SharePoint* | Get-ChildItem -Attributes D

Stop-Process -processname groove -Force
Stop-Process -processname msosync -Force
Stop-Process -processname msouc -Force
Stop-Process -processname csisyncclinet -Force
Stop-Process -processname winword -Force
Stop-Process -processname excel -Force
Stop-Process -processname powerpnt -Force
Stop-Process -processname mssync -Force

foreach ($folder in $SharePoint) {
    foreach ($name in $SharePointNames) {
        if($folder -match $name) {
        $folder | Remove-Item -Force
    }
  }
}

Remove-Item "$env:USERPROFILE\AppData\Local\Microsoft\Office\15.0\OfficeFileCache” -Recurse -Force
Remove-Item "$env:USERPROFILE\AppData\Local\Microsoft\Office\15.0\SPW” -Recurse -Force