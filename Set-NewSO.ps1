﻿function Set-NewSO() {

    <#
    .SYNOPSIS
    Sets a new SO for N-Able agent

    .DESCRIPTION
    Stops the Agent processes, modifies the applianceconfig.xml file twice, and then restarts services. 
    This is to hard change the SO and Customer ID that an agent possesses
    
    .PARAMETER CustomerID
    Specifies the new CustomerID

    .EXAMPLE
    Set-NewSO -CustomerID 201

    .NOTES
    @author Hashaw


    #>
    [CmdletBinding()] param(
        [int]$CustomerID
        
    )
    Write-Output "Stopping Services..."
    $Services = Get-Service -Name "Windows Agent*"
    $Services | Stop-Service -Force

    Write-Output "Stopping Agent..."
    Get-Process Agent* | Stop-Process -Force
    Get-Process Nable* | Stop-Process -Force

    Write-Output "Acquiring XML files..."
    $File = Get-ChildItem 'C:\Program Files (x86)\N-able Technologies' -Recurse -Filter "applianceconfig.xml"

    # Set this because I can't find a more elegant way to pass the file's location

    Write-Output "Modifying XML files..."
    foreach ($obj in $File) {
        $FileLocation = $obj.DirectoryName + "\" + $obj.Name
        $FileContent = $obj | Get-Content

        $FileContent = $FileContent -replace "<CustomerID>.+", "<CustomerID>$CustomerID</CustomerID>"
        $FileContent = $FileContent -replace "<ApplianceID>.+", "<ApplianceID>-1</ApplianceID>"
        $FileContent = $FileContent -replace "<CheckerLogSent>.+", "<CheckerLogSent>False</CheckerLogSent>"
        $FileContent = $FileContent -replace "<CompletedRegistration>.+", "<CompletedRegistration>False</CompletedRegistration>"

        $FileContent | Set-Content $FileLocation
    }

    Write-Output "Starting Services..."
    $Services | Start-Service
}