﻿
$folders = Get-ChildItem -Recurse -Directory C:\Support
$outputArray = @()
$pNames = @("Drive", "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "S11", "S12", "S13", "S14", "S15")
foreach ($item in $folders) {
    $obj = New-Object PSObject
    $row = $item.FullName.Split("\")
    for ($i = 0; $i -lt 16; $i++) {
        $obj | Add-Member -MemberType NoteProperty -Name $pNames[$i] -Value $row[$i]
    }
    $outputArray += $obj
    $obj = $null
}

$outputArray | Export-Csv C:\Support\test.csv -NoTypeInformation