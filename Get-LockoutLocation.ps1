﻿
function Get-LockoutLocation() {
    <#
    .SYNOPSIS
    Gets the server where user account lockouts are originating

    .DESCRIPTION
    Returns the server name of every lockout that a user has experienced


    .PARAMETER Username
    The user that is being locked out

    .EXAMPLE
    Get-LockoutLocation -Username "BRoss"

    .NOTES
    @author Unknown
    @modifiedBy Hashaw



    #>
    [CmdletBinding()] param(
        [string] $Username
    )
    $Pdce = (Get-AdDomain).PDCEmulator

    $GweParams = @{
     ‘Computername’ = $Pdce
     ‘LogName’ = ‘Security’
     ‘FilterXPath’ = "*[System[EventID=4740] and EventData[Data[@Name='TargetUserName']='$Username']]"
    }
    ## Query the security event log
    $Events = Get-WinEvent @GweParams
    Write-Output $Events
}