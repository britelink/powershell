﻿function New-Zip() {
    <#
    .SYNOPSIS
    Zips and removes folders within recursion depth

    .DESCRIPTION
    Takes Drive, Filter and Depth as parameters, then checks that specific depth for folders matching filter and zips them. Then the program removes them.
    Written in PS5
    Should work in PS4

    .PARAMETER Drive
    Which drive will you point this at.

    .PARAMETER Filter
    Filter for the specific folders you want to target

    .PARAMETER First
    Number of items you want to zip and delete.
    
    .PARAMETER Depth
    How deep you want to scan. Will only target that folder depth.

    .EXAMPLE
    New-Zip -Drive C:\ -Filter "New Folder" -Depth 2

    .NOTES
    @author Hashaw


    #>
    [CmdletBinding()] param(
        [string]$Drive,
        [string]$Filter,
        [int]$Depth
    )
    $emptyFolder = $Drive + "\empty"
    if (-Not (Test-Path $emptyFolder)){New-Item -Path $emptyFolder -ItemType Directory}
    $files = Get-ChildItem ($Drive.ToString() + "\*" * $Depth) -Filter $Filter | Where-Object { $_.PSIsContainer -and $_.Name -ne "empty" }
    if ($files.Length -eq 0)
    {
        Write-Verbose "No files matching filter at location, exiting"
    }
    $files | ForEach-Object{
        try {
            7z a ($_.FullName + ".zip") $_.FullName -tzip
            7z t ($_.FullName + ".zip") *.* -r
        } catch{
            Write-Error "Failed to zip folder: " + $_.FullName
            continue
        }
        Write-Debug "Removing folder: "
        Write-Debug $_.FullName
        try {
            
            Robocopy.exe $emptyFolder $_.FullName /purge
            
        } catch {
            Write-Error "Unable to remove $($_.Name)"
            Write-Error "Please remove manually"
        }
        Export-Csv -InputObject $_ -NoTypeInformation -Append ($Drive + "result.csv")
    }
    Remove-Item $emptyFolder -Force
}

 