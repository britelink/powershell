﻿# Requires Powershell Version 3+
#
#
#
#
# Get-StringHash is stolen code. 


Function Get-StringHash([String] $String,$HashName = "MD5") { 
$StringBuilder = New-Object System.Text.StringBuilder 
[System.Security.Cryptography.HashAlgorithm]::Create($HashName).ComputeHash([System.Text.Encoding]::UTF8.GetBytes($String)) | %{ 
[Void]$StringBuilder.Append($_.ToString("x2")) 
} 
$StringBuilder.ToString() 
}

function Get-FolderHash ($folder) {

    if ($folder[0] -eq "O") {
        $folder = $folder.replace("O:\", "\\apd-ras1\archive\")

    } elseif ($folder[0] -eq "R") {
        $folder = $folder.replace("R:\", "\\apd-ras1\archive2\")

    } elseif ($folder[0] -eq "K") {
        $folder = $folder.replace("K:\", "\\apdfp01\clients\")

    }
    $hashes = dir $folder -Recurse | Get-FileHash -Algorithm MD5
    $textToHash = ""
    foreach ($hash in $hashes.hash) {
        $textToHash += $hash
    }
    Get-StringHash $textToHash
}

#initialize variables and import file. Prime candidate for turning into a function and feeding in any input and output files. 
$scrub = Import-Csv "C:\Support\DuplicateScripts\RepeatFiles.csv"
$i = 0
$outputArray = @()
$checkagainst = @{}

ForEach($object in $scrub) {
    $i++
    $x = $i
    $objectTwo = $scrub[$x]
    $checkOne = 0
    
    #Required for export to csv. 
    $fileOne = "" | Select "Filename","Path","Hash","FolderHash"
    $fileTwo = "" | Select "Filename","Path","Hash","FolderHash"


    while ($object.'File Name' -eq $objectTwo.'File Name') {
        

        # Prime candidate for smaller function
        if ($checkAgainst.containsKey($object.'Path') -eq $FALSE) {
            $hash1 = Get-FolderHash $object.path
            $checkAgainst.add($object.'path', $hash1)
        } ELSE {
            $new = $object.path
            $hash1 = $checkAgainst.$new
        }

        if ($checkAgainst.containsKey($objectTwo.'Path') -eq $FALSE) {
            $hash2 = Get-FolderHash $objectTwo.path
            $checkAgainst.add($objectTwo.path, $hash2)
        } ELSE {
            $newTwo = $objectTwo.path
            $hash2 = $checkAgainst.$newTwo
        }

        
        if($hash1 -eq $hash2) {
            if ($checkOne -eq 0) {

                $fileOne.Filename = $object.'File Name'
                $fileOne.Path = $object.'Path'
                $fileOne.Hash = $object.'Hash'
                $fileOne.FolderHash = $hash1
                $outputArray += $fileOne
                $checkOne = 1
            }

            $fileTwo.Filename = $objectTwo.'File Name'
            $fileTwo.Path = $objectTwo.'Path'
            $fileTwo.Hash = $objectTwo.'Hash'
            $fileTwo.FolderHash = $hash2

            
            $outputArray += $fileTwo

            $outputArray | export-csv "C:\Support\DuplicateScripts\DuplicateFolders.csv" -NoTypeInformation -Append
            $outputArray = @()
            


        }
        #So no infinite loops occur.
        $objectTwo = $scrub[$x++]

                
    }
    #Clear everything up before the next cycle.
    $fileOne = $null
    $fileTwo = $null
    
}

# Known Bug: Second instance of duplicate folder gets inputted twice. No idea why. Doesn't harm data output at all.