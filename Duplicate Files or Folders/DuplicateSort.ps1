﻿$scrub = Import-Csv 'C:\Support\DuplicateScripts\Duplicate File report.csv'
$i = 0

ForEach($object in $scrub) {
    $i++
    $x = $i
    $objectTwo = $scrub[$x]
    $checkAgainst = @()

    while ($object.'File Name' -eq $objectTwo.'File Name') {
        

        if($object.Path -ne $objectTwo.Path -and $object.Path.Substring(1) -eq $objectTwo.Path.Substring(1)) {

            if($checkAgainst -contains $object.path -eq $FALSE) {

                $object | export-csv -NoTypeInformation -append C:\Support\DuplicateScripts\Repeatfiles.csv
                $checkAgainst += $object.path
                }

            if($checkAgainst -contains $objectTwo.path -eq $FALSE) {
                $objectTwo | export-csv -NoTypeInformation -append C:\Support\DuplicateScripts\Repeatfiles.csv
                $checkAgainst += $objectTwo.path }
                
        }


    $objectTwo = $scrub[$x++]

    
      }


    }