﻿#############################################################
#  WARNING: REQUIRES POWERSHELL 3.0 OR GREATER INSTALLED    #
#                                                           #
#                                                           #
#                                                           #
#                                                           #
#                                                           #
#############################################################

param(
    [int]    $days,
    [string] $emailFrom,
    [string] $smtpHost,
    [bool]   $smtpAuth,
    [int]    $portNum,
    [string] $contactInfo,
    [string] $smtpUsername,
    [string] $smtpPass,
    [int]    $passwordLength
)

if ($smtpAuth -ne $false -or $smtpAuth -ne $True) {$smtpAuth = $false};
$today          =  Get-Date 
$emailArray     =  @()
import-module ActiveDirectory


function Get-PasswordExpirationDays ($User) {
        (([datetime]::FromFileTime((Get-ADUser -Identity $User -Properties "msDS-UserPasswordExpiryTimeComputed")."msDS-UserPasswordExpiryTimeComputed"))-(Get-Date)).Days
}

$users = Get-ADUser -filter {Enabled -eq $True -and PasswordNeverExpires -eq $False} -Properties "DisplayName", "msDS-UserPasswordExpiryTimeComputed", "mail"

foreach ($user in $users) {
    $endTime = Get-PasswordExpirationDays $user."samaccountname"
    
    if ($endtime -le $days -and $endTime -gt 0) {
        $toEmail        =  New-Object psobject

        Add-Member -InputObject $toEmail -MemberType NoteProperty -Name User -Value ""
        Add-Member -InputObject $toEmail -MemberType NoteProperty -Name Email -Value ""
        Add-Member -InputObject $toEmail -MemberType NoteProperty -Name Expiry -Value ""
        Add-Member -InputObject $toEmail -MemberType NoteProperty -Name MailBody -Value ""
        Add-Member -InputObject $toEmail -MemberType NoteProperty -Name MailSubject -Value ""

        $name = $user."DisplayName"
        $toEmail.User = $user."DisplayName"
        $toEmail.Email = $user."mail"
        $toEmail.Expiry = $endTime
        $toEmail.MailBody = @"
         <h5><font face=Arial>Dear $name, </font></h5>
         <h5><font face=Arial>Your password will expire in $endTime days. <br />
         Your domain password is required for Computer Login, Remote Access, and Email Access.<br />
         To change your password when in the Calgary office, press CTRL-ALT-DEL and choose "Change a Password".<br />
         To change your password when logged in remotely, press CTRL-ALT-END and choose "Change a Password".<br />
         If you have a mobile phone with corporate email access, please remember to update your phone settings with your new password.<br />
         For your password to be valid it must be $passwordLength or more characters long and contain a mix of THREE of the following FOUR properties:<br /><br />
             uppercase letters (A-Z)<br />
             lowercase letters (a-z)<br />
             numbers (0-9)<br />
             symbols (!"$%^&*)<br /><br />
         $contactInfo<br /><br />
         Generated on: $today <br /><br />
          _____________ <br />
         <br /></font></h5>
"@
        $toEmail.MailSubject = $user."DisplayName" + ", your password will expire soon."

        if ($toEmail.User -ne "")
        {
            $emailArray += $toEmail
        }
    }
}
if($emailArray.Count -eq 0) {
    Write-Output "No users to email"
	exit
}
    If ($smtpauth -eq $True) {
        forEach ($user in $emailArray) {
        $creds = New-Object System.Management.Automation.PSCredential ( $smtpUsername, $smtpPass)
        Send-MailMessage -To $user.Email -From $emailFrom -Subject $user.MailSubject -BodyAsHtml $user.MailBody -SmtpServer $smtpHost -Port $portNum -UseSsl -Credential $creds
        }
    } else {
        forEach ($user in $emailArray) {
            Send-MailMessage -To $user.Email -From $emailFrom -Subject $user.MailSubject -BodyAsHtml $user.MailBody -SmtpServer $smtpHost -Port $portNum
        }
    }

Write-Output "Users emailed: "
Write-Output $emailArray.User

