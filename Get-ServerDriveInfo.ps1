﻿$servers = Get-ADComputer -Filter * | Where-Object {$_.Name -like "WWC*"}
[array]$collection = @()
$servers | ForEach-Object {
    $compName = $_.Name
    Get-WmiObject Win32_logicalDisk -ComputerName $compName | ?{ @(3) -contains $_.DriveType } | ForEach-Object {
        $object = New-Object System.Object
        $object | Add-Member -MemberType NoteProperty -Name "Server" -Value $compName
        $object | Add-Member -MemberType NoteProperty -Name "Disk" -Value $_.Name
        $object | Add-Member -MemberType NoteProperty -Name "Size" -Value ($_.Size / 1GB)
        $object | Add-Member -MemberType NoteProperty -Name "Free Space" -Value ($_.FreeSpace / 1GB)
        $collection += $object
    }
    
}