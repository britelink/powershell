﻿mkdir "C:\Support"
cd "C:\Support"
$url = "https://monitor.britelink.ca/dms/FileDownload?customerID=137&softwareID=101"
$output = "$PSScriptRoot\BritelinkAgent.exe"

$group = [ADSI]"WinNT://./Network Configuration Operators,group"
$group.Add("WinNT://User,user")
$group = [ADSI]"WinNT://./Users,group"
$group.Add("WinNT://User,user")
$adminGroup = [ADSI]"WinNT://./Administrators,group"
$adminGroup.Remove("WinNT://User,user")
Set-ExecutionPolicy RemoteSigned -Force
net user Administrator CAEAdmin!
net user Administrator /active:yes
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")

$app = Get-WmiObject -Class Win32_Product | Where-Object { 
    $_.Name -match "Office" 
}

$app | % { $_.Uninstall() }
cinst dotnet4.5.2 -y
cinst powershell -y
cinst jre8 -y 
cinst adobereader -y

$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Start-Process -FilePath "$PSScriptRoot\BritelinkAgent.exe" -ArgumentList "/s /passive /v'/qb'" -wait 

Write-Output "All finished! Please reboot"