﻿function Get-UserLastLogon() {
    <#
    .SYNOPSIS


    .DESCRIPTION


    .PARAMETER Drive


    .PARAMETER NewAbaqusServer

    
    .PARAMETER Port


    .EXAMPLE


    .NOTES
    @author Hashaw


    #>
    [CmdletBinding()] param(
        [string] $FilterString,
        [string] $Username
    )

    $Computers = Get-ADComputer -Filter {Name -Like $FilterString}

foreach ($comp in $Computers) 
{

Invoke-Command -ComputerName $comp.DNSHostNAme -ScriptBlock {get-winevent -FilterHashtable @{logname="security"; ID=4624;data=$Username} | select -first 1}
}

}