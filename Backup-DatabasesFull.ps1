﻿
# backup_full.ps1
# Full backup for database for specified SQL instance
#
# Change log:
# July 3, 2012: Thomas LaRock, http://thomaslarock.com
# Initial Version
# Get the SQL Server instance name from the command line
# leaving $dest null will result in default backup path being used
# May 17th 2016: Harlan Shaw
# Version 2 
# Made the script dump to a timestamp folder instead of 
# the jumble in a singular folder. Fixed comment errors.
param(
    [string]$inst=$null,
    [string]$dest=$null
    )
# Load SMO assembly, and if we're running SQL 2008 DLLs load the SMOExtended and SQLWMIManagement libraries
$v = [System.Reflection.Assembly]::LoadWithPartialName( 'Microsoft.SqlServer.SMO')
if ((($v.FullName.Split(','))[1].Split('='))[1].Split('.')[0] -ne '9') {
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.SMOExtended') | out-null
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.SQLWMIManagement') | out-null
    }
# Handle any errors that occur
Function Error_Handler {
Write-Host "Error Category: " + $error[0].CategoryInfo.Category
Write-Host "Error Object:  "  + $error[0].TargetObject
Write-Host "Error Message: "  + $error[0].Exception.Message
Write-Host "Error Message: "  + $error[0].FullyQualifiedErrorId
    }
Trap {
# Handle the error
    Error_Handler;
# End the script.
    break
}
 
$srv = new-object ('Microsoft.SqlServer.Management.Smo.Server') $inst
 
# If missing set default backup directory
 
If ($dest -eq "")
    { $dest = $inst.Settings.BackupDirectory + "\" };
    Write-Output ("Started at: " + (Get-Date -format yyyy-MM-dd-HH:mm:ss));
    $timestamp = Get-Date -format yyyy-MM-dd-HH-mm-ss;
    $dest = $dest + "\" + $timestamp + "\"
    Write-Host $dest
    New-Item -ItemType directory -Path ($dest)
    # Full-backup for every database
    foreach ($db in $srv.Databases) {
    If($db.Name -ne "tempdb") { # No need to backup TempDB 
        
        $backup = New-Object ("Microsoft.SqlServer.Management.Smo.Backup");
        $backup.Action = "Database";
        $backup.Database = $db.Name;
        $backup.Devices.AddDevice($dest + $db.Name + "_full_" + $timestamp + ".bak", "File");
        $backup.BackupSetDescription = "Full backup of " + $db.Name + " " + $timestamp;
        $backup.Incremental = 0;
        # Starting full backup process.
        $backup.SqlBackup($srv);
        };
    };
Write-Output ("Finished at: " + (Get-Date -format yyyy-MM-dd-HH:mm:ss));
