﻿Import-Module ActiveDirectory

$computers = Get-ADComputer -Filter {name -like "OUTPC*"}

foreach ($computer in $computers) {
    $computerName = $computer.Name
    Write-Output "Copying folder to $computerName..."
    Copy-Item "C:\Support\VCRedist" -Force -Destination "\\$computerName\C$\Support" -Recurse
    Write-Output "Installing VCRedist on $computerName"
    Invoke-Command -ComputerName $computerName -ScriptBlock {Start-Process "C:\support\VCRedist\vcredist_x64.exe" -Argumentlist "/q /norestart" -NoNewWindow -Wait; Start-Process "C:\support\VCRedist\vcredist_x86.exe" -Argumentlist "/q /norestart" -NoNewWindow -Wait; Start-Process "C:\support\VCRedist\vcredist_x64 (1).exe" -Argumentlist "/q /norestart" -NoNewWindow -Wait; Start-Process "C:\support\VCRedist\vcredist_x86 (1).exe" -Argumentlist "/q /norestart" -NoNewWindow -Wait}
    
}