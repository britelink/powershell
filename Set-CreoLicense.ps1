﻿function Set-CreoLicense() {

    <#
    .SYNOPSIS
    Sets the Creo program's license

    .DESCRIPTION
    The Set-CreoLicense function recursively loops through directories looking for the Creo license file, then modifies the file with the new license server location.

    .PARAMETER Drive
    Specifies the drive to check for Creo on the target server.
    Default C:\

    .PARAMETER NewCreoServer
    Specifies the new Creo License Server hostname

    .EXAMPLE
    Set-CreoLicense -Drive C:\ -NewCreoServer New-DCServer01 -Port 27000

    .NOTES
    @author Hashaw


    #>
    [CmdletBinding()] param(
        [string]$Drive = 'c:\', 
        [string]$NewCreoServer
    )
    
    
    $File = Get-ChildItem $Drive -Recurse -Filter "parametric.psf"

    $File | % {
    # Set this because I can't find a more elegant way to pass the file's location
    $FileLocation = $File.DirectoryName + "\" + $File.Name

    $FileContent = $File | Get-Content | % {$_ -replace "ENV=PTC_D_LICENSE_FILE-=.+", "ENV=PTC_D_LICENSE_FILE-=7788@$NewCreoServer"}

    $FileContent | Set-Content $FileLocation
    }

}


