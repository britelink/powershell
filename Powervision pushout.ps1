﻿New-PSDrive -Name "P" -PSProvider FileSystem -Root "\\OUT-FS1\Programs\"
Enable-PSRemoting -Force


Start-Process -FilePath msiexec -ArgumentList "/quiet /passive /qn /i C:\Support\Msodbcsql.msi IACCEPTMSODBCSQLLICENSETERMS=YES" -Wait
Copy-Item -Path "Microsoft.PowerShell.Core\FileSystem::\\Out-FS1\Programs\Powervision\pwv\Setup\Msodbcsql.msi" -Destination C:\Support

Invoke-Command -ComputerName OUTPC-003 -Credential $credentials -ScriptBlock {
robocopy \\Out-FS1\Programs\Powervision\pwv\Setup C:\Support msodbcsql.msi
ls C:\Support


}