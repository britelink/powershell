﻿cd "C:\Support\Nuance Install"

$app = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -match "Nuance Power PDF Advanced"}
$app.Uninstall()

msiexec.exe /i "System64\Nuance Power PDF Advanced.msi" TRANSFORMS="CWLLPNuance.mst" /quiet /norestart /qn 
cd Prerequisite
vc_redist2015.x64.exe /install /passive /norestart
vc_redist2015.x86.exe /install /passive /norestart