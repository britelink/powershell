﻿function Get-LogID() {
    <#
    .SYNOPSIS
    Returns all logs with chosen id in xml format

    .DESCRIPTION
    The Get-LogID function returns the complete listing of a windows Event via which log and ID you specify
    then converts it to xml and returns the result. Requires administrative access to run.

    .PARAMETER logName
    Specify which log you want to sort through such as "Security" or "Application"

    .PARAMETER idName
    Specify which idName you want to pull from the log. This is always a number.

    .EXAMPLE
    Get-LogID -logName Security -idName 4624

    .EXAMPLE
    Get-LogID -logName * -idName 1113

    .NOTES
    @author Harlan Shaw
    @version 1.0

    #>

    [CmdletBinding()] param (
    $logName,
    $idName
    )

    $Events = Get-WinEvent -FilterHashtable @{Logname=$logName;Id=$idName}
        ForEach ($Event in $Events) {            
            # Convert the event to XML
            $eventXML = [xml]$Event.ToXml()

    }
    return $eventXML.Event.EventData.Data
}