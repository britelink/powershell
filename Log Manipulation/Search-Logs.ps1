﻿function Search-Logs() {
    <#
    .SYNOPSIS
    Returns all information from all log files in selected folder.

    .DESCRIPTION
    Parses through log files in selected folder and returns output based on specified pattern

    .PARAMETER FolderName
    Specify the folder containing logs.

    .PARAMETER Pattern
    Designate pattern for search string. Can input multiple items via an array. Can also take regular expressions.

    .EXAMPLE
    Search-BentleyLogs -FolderName C:\Softrack\Logs\Audit -Pattern \#\^\#

    .EXAMPLE
    Search-BentleyLogs -FolderName C:\Softrack\Logs\Audit -Pattern \#\^\# | Export-CSV name.csv

    .NOTES
    @author Harlan Shaw
    @version 1.0

    #>

    [CmdletBinding()] param (
    $FolderName,
    $Pattern
    )
    Get-ChildItem -Path $FolderName | % {
        Write-Output Select-String -InputObject $_ -Pattern $Pattern

        }
    return $null


}