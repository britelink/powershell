﻿function New-CorporateADUser() {
    <#
    .SYNOPSIS
    Creates a user, with a UPN, a password, and an email address.

    .DESCRIPTION


    .PARAMETER Drive


    .PARAMETER NewAbaqusServer

    
    .PARAMETER Port


    .EXAMPLE


    .NOTES
    @author Hashaw


    #>
    [CmdletBinding()] param(
        [string] $username,
        [string] $password,
        [string] $emailServer,
        [string] $OU
    )
    if (-not (Get-Module ActiveDirectory)){            
        Import-Module ActiveDirectory            
    }      

    $domain = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()            
    $domaindn = ($domain.GetDirectoryEntry()).distinguishedName

}