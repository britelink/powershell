﻿#requires -version 2
#Get-FolderLastWrittenTime meta script for
#N-Able
#
Param(
    [Parameter(Mandatory=$true)][string]$Folder
)
#Write-Verbose "Attempting to download requisite scripts from source"
#try {
#    iex ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/britelink/powershell/raw/master/Get-FolderLastWrittenTime.ps1'))
#} catch {
#    throw "Could not retrieve scripts from source"
#}

Get-ChildItem $Folder | ?{ $_.PSIsContainer } | Select FullName,LastWriteTime