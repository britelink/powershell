﻿#requires -version 2
# Zip folder Meta-script
#
#
Param(
  [Parameter(Mandatory=$true)][string]$Folder,
  [Parameter(Mandatory=$true)][string]$Filter,
  [Parameter(Mandatory=$true)][int]$Depth
)

Write-Verbose "Attempting to download and run script from source control."
try 
{
    Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/britelink/powershell/raw/master/New-Zip.ps1'))
} catch
{
    throw "Could not download and run script from source."
}

Invoke-Command -ScriptBlock {New-Zip -Drive $Folder -Filter $Filter -Depth $Depth} -Verbose -NoNewScope