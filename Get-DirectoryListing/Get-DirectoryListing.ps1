﻿
function Get-DirectoryListing() {
    <#
    .SYNOPSIS
    Returns a listing of all directories to specific depth

    .DESCRIPTION
    Returns a listing of all directories to a specific depth

    .PARAMETER Path
    Specifies the drive to check for Abaqus on the target server.
    Default C:\

    .PARAMETER Depth
    Returns the number of folders deep the output array contains

    .EXAMPLE
    Get-DirectoryListing -Path C:\ | Export-CSV C:\Test.csv -NoTypeInformation

    .NOTES
    @author Hashaw


    #>
    [CmdletBinding()] param(
        [string]$Path = 'c:\',
        [int]$Depth = 8

    )
    $folders = Get-ChildItem -Recurse -Directory $Path
    $outputArray = @()
    $pNames = @("Drive")
    for ($i = 0; $i -lt $Depth + 1; $i++) {
        $pNames += "S" + $i;
    }
    
    foreach ($item in $folders) {
        $obj = New-Object PSObject
        $row = $item.FullName.Split("\")
        for ($i = 0; $i -lt $Depth + 1; $i++) {
            $obj | Add-Member -MemberType NoteProperty -Name $pNames[$i] -Value $row[$i]
    }
    $outputArray += $obj
    $obj = $null
    }

    return $outputArray


}

