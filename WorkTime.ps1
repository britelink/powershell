﻿function workTime () 
{
    Start-Process "chrome.exe" "https://connect.britelink.ca:9050"
    Start-Process "chrome.exe" "https://www.autotask.net/Mvc/Framework/Authentication.mvc/Authenticate"
    Start-Process "firefox.exe" "https://monitor.britelink.ca/"
    Start-Process "C:\Program Files\OpenVPN\bin\openvpn-gui.exe" "--connect pfSense-udp-1196-harlan-config.ovpn"
    Start-Process "I:\Applications\emacs\bin\emacsclientw.exe" "-c -n"
    Start-Process "outlook.exe"
    Start-Process "C:\Program Files (x86)\Microsoft Office\root\Office16\lync.exe"
}