setlocal enableextensions enabledelayedexpansion
::Switches:
::(E)mpty folders included in copy
::(R)etry each copy up to 15 times
::(W)ait 5 seconds between attempts
::(LOG) creates log file
::(NP) do not include progress txt in logfile; this keeps filesize down
::(MIR)rors a directory tree
::(MT[:n]) Do multi-threaded copies with n threads (default 8).

::Source path
set sourcepath=\\10.0.1.140\c$\Support\FolderYouWantMovedToAPD

::Destination path
set destinationpath=C:\Support\RobocopyTest
if not exist %destinationpath% mkdir %destinationpath%

::Log path
set logpath=C:\Support\RobocopyTest
if not exist %logpath% mkdir %logpath%

::Credentials
set username=toughbook4\Field_user4
set password=apd

::Include format yyyy-mm-dd#hh-mm-ss.ms in log filename
set filename=Weekly-v2_%date:~-4,4%-%date:~-10,2%-%date:~-7,2%#%time::=-%.txt

::Run command
::robocopy %sourcepath% %destinationpath% /E /R:15 /W:5 /LOG:"%logpath%%filename%" /NP /MIR

::So that we can talk to the remote host
net use %sourcepath% /user:%username% %password%

robocopy %sourcepath% %destinationpath% /MIR /LOG:"%logpath%%filename%" /E /R:15 /W:5 /NP
