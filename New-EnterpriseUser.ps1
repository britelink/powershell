﻿function New-EnterpriseUser() {
    <#
    .SYNOPSIS
    Creates a new user and mailbox

    .DESCRIPTION
    Must be run on a Domain Controller. Imports Active Directory modules, creates new user from
    parameters, then connects to an Exchange server and provisions a mailbox for them under specific Database.

    .PARAMETER SAMName
    The SAM name that you want the user to have

    .PARAMETER FullName
    The user's Full name, does not accept Middle names
    
    .PARAMETER MailServer
    The target mail server.

    .PARAMETER OU
    The OU that you would like the User's AD account to be created in.

    .PARAMETER Database
    The Email database you would like to provision the user's email mailbox in.

    .PARAMETER Password
    The user's new password

    .PARAMETER HomeDrive
    The user's Home Drive letter

    .PARAMETER HomeDirectory
    The user's home directory, supports UPN paths.

    .EXAMPLE
    New-EnterpriseUser -SAMName TestUser -FullName "Test User" -MailServer Test-EXCH01 -OU 'CN=Users,DC=fabrikam,DC=local' -Password "NotEncryptedTotallyFixMe" -Database TestDB

    .NOTES
    @author Gihoveg


    #>
    [CmdletBinding()] param(
        
        [Parameter(Mandatory=$True)]
        [string] $SAMName,
        [Parameter(Mandatory=$True)]
        [string] $FullName,

        [string] $MailServer,
        [string] $OU,
        [string] $Database,
        [string] $Password,
        [string] $HomeDrive,
        [string] $HomeDirectory
    )

    Import-Module ActiveDirectory

    #Cleanup and getting requisite variables
    $firstName = ($FullName.trim().split(' '))[0]
    $lastName = ($FullName.trim().split(' '))[1]
    $domain = Get-ADDomain
    $SAMName = $SAMName.tolower();

    if ($HomeDirectory.Substring($HomeDirectory.Length -1, 1) -eq "\")
    {
        $HomeDirectory += $SAMName
    }
    else
    {
        $HomeDirectory += "\" + $SAMName
    }

    #My testing errored out with multiple Domain Controllers unless I specified this, forcing the Mailbox creation to check the Domain Controller
    #that the script is using to create the user account (Before synchronization)
    $domainController = (Get-ADDomainController).Name

    #Setup Parameters to add into New-ADUser. Cleaner than directly writing them out, or using backticks
    $params = @{
        Name              = $FullName.trim();
        SamAccountName    = $SAMName.trim();
        UserPrincipalName = ($SAMName.trim() + "@" + $domain.DNSRoot);
        GivenName         = $firstName;
        Surname           = $lastName;
        HomeDirectory     = $HomeDirectory;
        HomeDrive         = $HomeDrive;
        Server            = $domainController
    }

    #Adding if it exists
    if ($OU) {$params.Add("Path", $OU)}
    #Password if it exists
    if ($Password) {
        $params.Add("AccountPassword", (ConvertTo-SecureString -String $Password -AsPlainText -Force))
        $params.Add("Enabled", $True)
    }
    
    try {
        Write-Debug "Creating user account"
        $newUserInfo = New-ADUser @params -PassThru
    } Catch {
        throw $error[0]
    }

    If ($MailServer) {
        try {
            Write-Debug "Connecting to Exchange server"
            $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri ("http://" + $MailServer + "." + $domain.DNSRoot + "/PowerShell/") -Authentication Kerberos
            Import-PSSession $Session -AllowClobber
            } Catch {
                Write-Error "Unable to connect to Exchange server"
                break
            }

            $mailParams = @{
                Identity = $params.SamAccountName
                DomainController = $domainController
            }

            if($Database) {$mailParams.Add("Database", $Database)}
            Write-Debug "Creating mailbox."
          Enable-Mailbox @mailParams
          Write-Debug "Retrieving SMTP information"
          $userMailbox = Get-Mailbox -Identity $params.SamAccountName -DomainController $domainController
          Write-Debug "Setting last of user properties"
          
          try 
          {
            Set-ADUser -Identity $params.SamAccountName -EmailAddress $userMailbox.PrimarySMTPAddress -Server $domainController -UserPrincipalName $userMailbox.PrimarySMTPAddress
          } 
          catch
          {
            Write-Error "Unable to set UPN to email address"
            Set-ADUser -Identity $params.SamAccountName -EmailAddress $userMailbox.PrimarySMTPAddress -Server $domainController
          }           
    }

}