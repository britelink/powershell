﻿Import-Module ActiveDirectory
$ErrorActionPreference= 'silentlycontinue'
Get-ADComputer -Filter 'OperatingSystem -like "*"' -Properties * | Select-Object Name |
ForEach-Object {
    If (Test-Connection $_.Name -Count 1){
        Get-HotFix -ComputerName $_.Name | Sort-Object InstalledOn -Descending | Select-Object -First 1 
    }
    else {
        Write-host $_.Name " Connection Error"
    }
} |
Sort-Object InstalledOn