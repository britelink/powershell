﻿# Mailbox deletion
$users = Get-Content C:\Support\users.txt

$users | % { New-MailboxExportRequest -Mailbox $_ -FilePath \\SWR-EXCH01\F$\Support\$_.pst }

$users | % { Remove-Mailbox -Identity $_ }
